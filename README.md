# Duck generator

DUCK (Declarative Ultimate Correspondance Kit) is a framework for sharing remote objects and making remote call procedures in a distributed system over UDP.

It was created to fill the requirements of a distributed system course at NTU (Singapore). Using an existing framework was not allowed so here is a new one !

## How does it works?

### Remote object descriptive file
Objects properties are described in a file, and then DUCK can generate the code in the wanted language. For now, only `Kotlin` and partially `Node.JS` are supported.

Nowadays there is no need for keywords such as `class` or `return` when you have so many emojis to represent it ! That's why the syntax of descriptive file is using emojis.

_(The emoji keyboard for DUCK does not yet exists but will surely be created when the standard reach everyone.)_ 

### File example
Here is a sample of the syntax of a duck file (you can notice we don't need syntax coloration here because of emojis !) :
```
🦆 File
	Metadata metadata
	long timestamp

	💧 readFile
		long offset
		long size
		👉 string

	💧 writeFile
		long offset
		string content
	
	💧 delete

🍞 Metadata
	string name
	long size
```

Here `File` is a remote object with 2 fields (`timestamp` and `metadata`), and 3 functions (`readFile`, `writeFile` and `delete`).

Metadata a regular object (similar to structure in C) with 2 fields, defining it here allow to be sure that every client will have an implementation, it is however a simple data class (in Kotlin at least) so you can define you own if you're only using Kotlin.

## Generating code

This project contains a maven module, it is only hosted on this project GitLab repository, so the easiest way to use it now is to add the maven repository in the pom of your project.

The following example generate the interface defined in `src/main/resources/interface.duck` and output the result into `target/generated-sources/duck/`.

```xml
<project>
    <!-- ... -->
    <pluginRepositories>
        <!-- ... -->
        <pluginRepository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/16881175/packages/maven</url>
        </pluginRepository>
        <!-- ... -->
    </pluginRepositories>
    <!-- ... -->
    <build>
        <plugins>
            <!-- ... -->
            <plugin>
                <groupId>io.github.banilaste.duck</groupId>
                <artifactId>duck-generator</artifactId>
                <version>1.0-SNAPSHOT</version>
                <executions>
                    <execution>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>generate-sources</goal>
                        </goals>
                        <configuration>
                            <input>${project.basedir}/src/main/resources/interface.duck</input>
                            <output>${project.build.directory}/generated-sources/duck</output>
                            <language>kotlin</language>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- ... -->
        </plugins>
    </build>
</project>
```

Alternatively to using the repository, you can install the plugin into your local maven repository.
```sh
git clone https://gitlab.com/banilaste/duck.git
cd duck
mvn install
```

## Usage of generated files

The generated files do not work without the associated network library, which you should include in your maven project.

For example in kotlin (here again you should include the project maven repository, this time using `<repositories>` instead of `<pluginRepositories>`) :
```xml
<dependency>
    <artifactId>duck-kotlin</artifactId>
    <groupId>io.github.banilaste.duck</groupId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

Refer to your language's DUCK library to use the generated output. Once all dependencies included, it should be as simple as extending the generated interfaces and having a first fixed reference to a remote server's object.

- Kotlin : [https://gitlab.com/banilaste/duck-kotlin](https://gitlab.com/banilaste/duck-kotlin)
- Node : [https://gitlab.com/banilaste/duck-node](https://gitlab.com/banilaste/duck-node)

## Examples

A simple file sharing system was created using this library, you can check the :
- kotlin version : [https://gitlab.com/banilaste/file-share](https://gitlab.com/banilaste/file-share)
- node version (client only) : [https://gitlab.com/banilaste/file-share-node](https://gitlab.com/banilaste/file-share-node)

