import { duckProperties, RemoteObject, DatagramHandler, RemoteObjectIdentifier } from "duck-node";
import {
	{% for name, class in remote.items() %}
		{{ ", " if not loop.first }}
		{{ name }}Stub
	{% endfor %}
} from "./remote-stubs";

namespace DuckProject {
	export function init(localPort: number = 33333) {
		{% for name, class in remote.items() %}
			RemoteObject.factories["{{ class.name }}"] =
				(it: RemoteObjectIdentifier) => new {{ class.name }}Stub(it);
		{% endfor %}
		duckProperties.localPort = localPort;
		DatagramHandler.init()
	}
}

export default DuckProject;