package io.github.banilaste.duck.generated

import io.github.banilaste.duck.*
{% for name, class in remote.items() %}
@RemoteInterface
abstract class {{ name }}(id: RemoteObjectIdentifier): RemoteObject(id) {
    constructor() : this(localIdentifier())

	{%
		for field in class.fields
	%}@RemoteMember({{ field.id }})
	abstract var {{ field.name }}: {{ field | type }}
	{{ "protected set" if field.readonly }}
	{%
		endfor
	%}{%
		for func in class.functions
	%}
	@RemoteMember({{ func.id }}){% if func.noCaching %}
	@NoCaching
	{% endif %}abstract fun {{ func.name }}({%
		for input in func.inputs
			%}{{ ", " if not loop.first }}{{ input.name }}: {{ input | type }}{%
		endfor
	%}){% if func.return %}: {{ func.return | type }}{% endif %}
	{%
		endfor
	%}
}
{% endfor %}