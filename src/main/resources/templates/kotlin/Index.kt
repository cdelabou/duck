package io.github.banilaste.duck.generated

import io.github.banilaste.duck.DuckProperties
import io.github.banilaste.duck.RemoteObject
import io.github.banilaste.duck.network.DatagramHandler

object DuckProject {
	fun init(localPort: Int = 33333) {
		{% for name, class in remote.items()
			%}RemoteObject.factories[{{ class.name }}::class] = { {{ class.name }}Stub(it) }
		{% endfor %}
		DuckProperties.localPort = localPort
		DatagramHandler.init()
	}
}