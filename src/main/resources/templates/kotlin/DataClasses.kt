package io.github.banilaste.duck.generated
{% for name, class in data.items() %}
data class {{ name }} ({%
	for field in class.fields
		%}{{ ", " if not loop.first }}var {{ field.name }}: {{ field | type }}{%
	endfor %})
{% endfor %}