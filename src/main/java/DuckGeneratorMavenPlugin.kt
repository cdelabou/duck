package io.github.banilaste.duck.generator

import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter

@Mojo(name = "generate-sources", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
class DuckGeneratorMavenPlugin: AbstractMojo() {
    @Parameter(property = "input", defaultValue = "index.duck")
    private var input: String? = null

    @Parameter(property = "language", defaultValue = "kotlin")
    private var language: String? = null

    @Parameter(property = "output", defaultValue = "")
    private var output: String? = null

    override fun execute() {
        Generator(input!!, output!!, language!!)
            .generate()
    }
}