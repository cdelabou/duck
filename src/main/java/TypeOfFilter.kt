package io.github.banilaste.duck.generator

import com.hubspot.jinjava.interpret.JinjavaInterpreter
import com.hubspot.jinjava.lib.filter.Filter

interface LanguageFilter {
    fun realTypeNameOf(dataType: DataType, precise: Boolean, arrayType: Boolean): String
}

enum class LanguageFilters: LanguageFilter {
    KOTLIN {
        override fun realTypeNameOf(dataType: DataType, precise: Boolean, arrayType: Boolean): String {
            var typeString = when (dataType.type) {
                "long" -> "Long"
                "string" -> "String"
                "int" -> "Int"
                "double" -> "Double"
                "float" -> "Float"
                "boolean" -> "Boolean"
                "bytes" -> "ByteArray"
                else -> dataType.type
            }

            if (arrayType) {
                repeat(dataType.arrayDepth) {
                    typeString = "List<${typeString}>"
                }
            }

            return typeString + if (precise && dataType.nullable) "?" else ""
        }
    },
    NODE {
        override fun realTypeNameOf(dataType: DataType, precise: Boolean, arrayType: Boolean): String {
            var typeString = if (precise) {
                when (dataType.type) {
                    "long" -> "bigint"
                    "string" -> "string"
                    "int" -> "number"
                    "double" -> "number"
                    "float" -> "number"
                    "boolean" -> "boolean"
                    "bytes" -> "Buffer"
                    else -> dataType.type
                }
            } else {
                when (dataType.type) {
                    "long", "string", "int", "double", "float", "boolean", "bytes" -> "RawType.${dataType.type.toUpperCase()}"
                    else -> dataType.type
                }
            }

            if (precise && arrayType) {
                repeat(dataType.arrayDepth) {
                    typeString = "${typeString}[]"
                }
            }

            return typeString + if (precise && dataType.nullable) " | null" else ""
        }
    }
}

/**
 * Map a string with the matching language type
 *
 * Useful to match the default types of DUCK and decorate types with array
 */
class TypeOfFilter(filter: LanguageFilter): Filter, LanguageFilter by filter {
    override fun getName(): String {
        return "type"
    }

    override fun filter(content: Any?, p1: JinjavaInterpreter?, vararg p2: String?): Any? {
        val precise = p2.isEmpty() || p2[0] == "true"
        val arrayType = p2.size < 2 || p2[1] == "true"
        
        return when (content) {
            is Argument -> realTypeNameOf(content.dataType, precise, arrayType)
            is DataType -> realTypeNameOf(content, precise, arrayType)
            else -> content
        }
    }

}