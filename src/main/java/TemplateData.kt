package io.github.banilaste.duck.generator

/**
 * All data structures used in the template
 */
data class ClassFunction (
    val id: Int,
    val name: String,
    val noCaching: Boolean,
    val inputs: MutableList<Argument>,
    var `return`: DataType? = null
)

data class DataType (
    val type: String,
    val arrayDepth: Int = 0,
    val nullable: Boolean
)

open class Argument (
    val dataType: DataType,
    val name: String
)

class Field (
    val id: Int,
    dataType: DataType,
    name: String,
    val readonly: Boolean
): Argument(dataType, name)

open class DataClass (
    val name: String,
    val fields: MutableList<Field>
)

class Class (
    name: String,
    fields: MutableList<Field>,
    val functions: MutableList<ClassFunction>
): DataClass(name, fields)

data class Result(
    val remoteClasses: HashMap<String, Class>,
    val dataClasses: HashMap<String, DataClass>
)