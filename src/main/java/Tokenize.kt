package io.github.banilaste.duck.generator

/**
 * Different kinds of tokens
 */
enum class TokenType(val text: String?) {
    CLASS("\uD83E\uDD86"), // 🦆
    DATA_CLASS("\uD83C\uDF5E"), // 🍞
    FUNCTION("\uD83D\uDCA7"), // 💧
    RETURN("\uD83D\uDC49"), // 👉
    READONLY("\uD83D\uDC41"), // 👁
    NULLABLE("\u2753"), //❓
    ARRAY("\uD83D\uDE9F"), // 🚟
    NO_CACHE("\uD83D\uDD01"), // 🔁
    
    EOF(null), // null so it can't be matched
    TEXT(null)
}

data class Token(val type: TokenType, val content: String? = null)

/**
 * Convert a string into list of tokens
 */
fun tokenize(content: String): MutableList<Token> {
    return (
            content.split(Regex("\\s+"))
                .filter { it != "" }
                .map { text ->
                    val matchingToken = TokenType.values().find { it.text == text }

                    // Only 2-characters strings can be special tokens (represented by 2 chars in utf-8)
                    if (matchingToken != null) {
                        Token(matchingToken)
                    } else {
                        Token(TokenType.TEXT, text)
                    }
                }
                    + listOf(Token(TokenType.EOF))
            ).toMutableList()
}