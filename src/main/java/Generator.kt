package io.github.banilaste.duck.generator

import com.hubspot.jinjava.Jinjava
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Paths

class Generator(val input: String, val output: String, val language: String) {
    data class NamedTemplate(val name: String, val template: String)

    private fun readTemplates(language: String): List<NamedTemplate> {
        val folder = "/templates/$language"
        val classLoader = Generator::class.java
        val uri = classLoader.getResource(folder)?.toURI()
            ?: throw Exception("unsupported language : $language")

        // Inside jar or inside IDE
        return if (uri.scheme.contains("jar")) {
            val jarUrl = classLoader.protectionDomain.codeSource.location.toURI()
            val fs = FileSystems.newFileSystem(Paths.get(jarUrl), null)
            Files.newDirectoryStream(fs.getPath(folder)).map {
                val res = classLoader.getResourceAsStream(it.toString())!!
                NamedTemplate(
                    it.fileName.toString(),
                    BufferedReader(InputStreamReader(res, StandardCharsets.UTF_8)).readText()
                )
            }
        } else {
            File(uri).listFiles()!!.map {
                NamedTemplate(it.name, it.readText())
            }
        }
    }

    fun generate() {
        // Retrieve and parse content
        val content = File(input).let {
            if (it.exists() && it.isFile) {
                it.readText(Charsets.UTF_8)
            } else {
                throw Exception("unable to find file $input")
            }
        }

        val result = TreeBuilder(tokenize(content)).build()

        // Create context and init jinjava
        val context = HashMap<String, Any>()
        context["remote"] = result.remoteClasses
        context["data"] = result.dataClasses

        val jinjava = Jinjava()
        // Add conversion filter associated with language
        LanguageFilters.values().find { it.name == language.toUpperCase() }
            ?.let {
                jinjava.globalContext.registerFilter(TypeOfFilter(it))
            }

        // Create output folder
        Files.createDirectories(Paths.get(output))

        // Read templates
        readTemplates(language).forEach { file ->
            // Apply content
            val render = jinjava.render(file.template, context)

            // Write output
            File(output.let { if (it.endsWith("/")) it else "$it/" } + file.name)
                .writeText(render, Charsets.UTF_8)
        }
    }
}

/**
 * Retrieve properties from command line arguments
 */
fun parseProperties(args: Array<String>): MutableMap<String, String> {
    val properties = HashMap<String, String>()
    properties["language"] = "kotlin"
    properties["output"] = ""

    args.forEach {
        if (it.startsWith("--")) {
            val (left, right) = it.substring(2).split(Regex("="), 2) + arrayOf("")
            properties[left] = right
        }
    }

    if (!properties.containsKey("input")) {
        throw Exception("no input file specified")
    }


    return properties
}




fun main(args: Array<String>) {
    val properties = parseProperties(args)
    Generator(properties["input"]!!, properties["output"]!!, properties["language"]!!)
        .generate()
}